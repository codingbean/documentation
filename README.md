# Rainwater flat system

In this documentation I will put my ideas, drawing, thoughts etc. about the rainwater system.
The goal of the system is to use only collected rainwater to water platns, filling teracce pool, cleaning teracce, cleaning windows.
Next step is to use these water in toilet (about 35% used water in house is the toilet usage).

Water will be kept in wooden or metal barrel (cost about 300 zł for 300 liters capacity).
Pumps, computer(rpi zero or arduino) will get electricity from acumulator.
Acumulator will be charged from solar panel.

## Costs:

| What   | Cost | Currency | URL |
| ------ | ---- | -------- | --- |
| barrel | 300  | PLN      |     |
| solar set (acumulator,solar panel, charging regulator) | 555 | PLN | [suntrack.pl](https://suntrack.pl/zestawy-baterie-sloneczne/1004-baterie-sloneczne.html) |
| pump 12V 240l/h | 50 | PLN | allegro.pl |
| water sensor | 5 | PLN | [abc-rc.pl](https://abc-rc.pl/detektor-wody-fc-37) (4x5 cm), [aliexpres.com](https://pl.aliexpress.com/item/Rain-Water-Level-Sensor-Module-Detection-Liquid-Surface-Depth-Height-For-Arduino/32693896961.html) (4x2 cm) |
| Raspberry Pi Zero W | 11 | EUR | [kiwi-electronics.nl](https://www.kiwi-electronics.nl/raspberry-pi-zero/raspberry-pi-zero-w?lang=en), [modmypi.com](https://www.modmypi.com/raspberry-pi/raspberry-pi-zero-board-379/rpi-zero-board/raspberry-pi-zero), [thepihut.com](https://thepihut.com/products/raspberry-pi-zero-w?variant=30333236753)
| SUMMARY | ~1000 | PLN ||

## Calculations of benefits

It is said that using rainwater to water plants is healthy for the plants. They use water from them environment.
But how many water I can collect from the taracce. All tarace has about 100 m^2. My part has about 64 m^2. On the taracce three are drainings and on my part two of them. In the first step I will use one, the samller one where I can put a barrel. The second one is covered by big flower boxes, and the access to the draining is not good. So working area for the first phase is about 35 m^2. In wrocław rain give about 550-600 liter per quare meter per year water.
550 * 35 * 0.8 = 15400 liters/yearly
Usage of water is about 3000l for water plants and another 3000l for cleaning and other stuff.
(6000+15400)/2 * 21/365 = 615 - size of barrel for water